default:
	cd lib; make

clean:
	cd lib; make clean

distclean:
	cd lib; make distclean

install:
	cd lib; make install
	
