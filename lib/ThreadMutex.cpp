#include "ThreadMutex.h"

using rf152::ThreadMutex;

ThreadMutex::ThreadMutex() {
	pthread_mutex_init(&_mutex, NULL);

}

ThreadMutex::~ThreadMutex() {
	pthread_mutex_destroy(&_mutex);
}

void ThreadMutex::lock() {
	pthread_mutex_lock(&_mutex);
}

void ThreadMutex::unlock() {
	pthread_mutex_unlock(&_mutex);
}

bool ThreadMutex::tryLock() {
	if (pthread_mutex_trylock(&_mutex) == 0) {
		return true;
	} else {
		return false;
	}
}
