/**
 * @file Thread.h
 * @author rf152
 *
 */
#ifndef THREAD_H_
#define THREAD_H_

#include "rf152.h"
#include <pthread.h>

/**
 * @brief OO Thread class using pthreads
 *
 * Object Oriented class for managing threads. Creating a thread is as easy
 * as inheriting this class and implementing the threadExecute() function.
 */
class rf152::Thread {
public:
	Thread();

	~Thread();

	/**
	 * Start the thread going
	 */
	void start();
	/**
	 * Mark the thread as to be stopped.
	 */
	void stop();
	/**
	 * Blocking function to check whether the thread has returned. Used if you
	 * want to ensure the thread has terminated before exiting a program.
	 */
	void stopwait();
	/**
	 * Check whether the thread is in a running state
	 * @return
	 */
	bool isRunning();
	/**
	 * Do the actual running of the thread
	 */
	static void *threadEntry(void*);

protected:
	/**
	 * Check whether to kill the thread.
	 * @return true if kill signal has been recieved, false otherwise
	 */
	bool threadKillCheck();
	/**
	 * The main body of the thread. This code will be executed when the thread
	 * is started.
	 *
	 * It should implement calls to threadKillCheck() to see whether or not
	 * it has been asked to terminate
	 */
	virtual void threadExecute() = 0;

private:
	pthread_t _pthread;
	pthread_attr_t _pattr;
	rf152::ThreadMutex *_stopMutex;
	bool _toStop;
	bool _isActive;
};

#endif /* THREAD_H_ */
