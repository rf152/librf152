/**
 * @file ThreadMutex.h
 * @author rf152
 */
#ifndef THREADMUTEX_H_
#define THREADMUTEX_H_

#include "rf152.h"
#include <pthread.h>

/**
 * @brief OO Class for mutexes
 *
 * Object Oriented class for handling thread mutexes
 */
class rf152::ThreadMutex {
public:
	/**
	 * Constructor
	 * @return
	 */
	ThreadMutex();
	/**
	 * Destructor
	 * @return
	 */
	virtual ~ThreadMutex();
	/**
	 * Lock function. Will block until the mutex is available
	 */
	void lock();
	/**
	 * Unlock function. Will unlock the mutex
	 */
	void unlock();
	/**
	 * Try to lock the mutex.
	 * @return True if locked, False otherwise;
	 */
	bool tryLock();
private:
	pthread_mutex_t _mutex;
};

#endif /* THREADMUTEX_H_ */
