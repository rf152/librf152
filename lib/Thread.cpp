#include "Thread.h"
#include "ThreadMutex.h"
#include <unistd.h>

using rf152::Thread;
using rf152::ThreadMutex;
using namespace std;

Thread::Thread() {
	_stopMutex = new ThreadMutex();
	_toStop = false;
	pthread_attr_init(&_pattr);
    pthread_attr_setdetachstate(&_pattr, PTHREAD_CREATE_DETACHED);
}

Thread::~Thread() {
	delete _stopMutex;
	pthread_attr_destroy(&_pattr);
	stop();
	stopwait();
}

void Thread::start() {
	if (_isActive) return;
	int ret;
	ret = pthread_create(&_pthread, &_pattr, Thread::threadEntry, (void*)this);
	return;
}

void Thread::stop() {
	_stopMutex->lock();
	_toStop = true;
	_stopMutex->unlock();
}

void Thread::stopwait() {
	while (_isActive) {
		usleep(100);
	}
}

bool Thread::isRunning() {
	return _isActive;
}

bool Thread::threadKillCheck() {
	bool stopMe;
	_stopMutex->lock();
	stopMe = _toStop;
	_stopMutex->unlock();
	return stopMe;
}

void *Thread::threadEntry(void* callingthread) {
	Thread *pt = (Thread*)callingthread;

	pt->_isActive = true;
	pt->_toStop = false;
	pt->threadExecute();
	pt->_isActive = false;

	pthread_exit(0);
}
